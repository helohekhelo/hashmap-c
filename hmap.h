#ifndef __HMAP_H
#define __HMAP_H

#include "def.h"
#include "list.h"

#include <stddef.h>
#include <stdlib.h>

#define HMAP_DEFAULT_BUCKETS 8

struct hmap_bucket {
	struct list_head *first, *last;
};

typedef int (*hmap_equal_f)(struct list_head *, struct list_head *);
typedef u32 (*hmap_hash_f)(struct list_head *);

struct hmap {
	struct list_head entries;
	struct hmap_bucket *buckets;
	size_t cnt, cap;

	hmap_equal_f equal_fn;
	hmap_hash_f hash_fn;
};

void hmap_init(struct hmap *map, hmap_equal_f equal_fn, hmap_hash_f hash_fn);

static inline struct hmap *hmap_new(hmap_equal_f equal_fn, hmap_hash_f hash_fn)
{
	struct hmap *map = malloc(sizeof(*map));
	if (map == NULL)
		return NULL;
	hmap_init(map, equal_fn, hash_fn);
	return map;
}

struct list_head *hmap_add(struct hmap *map, struct list_head *head);

struct list_head *__hmap_get(struct hmap *map,
		struct list_head *head, struct hmap_bucket **bucket);

static inline struct list_head *hmap_get(struct hmap *map,
		struct list_head *head)
{
	struct hmap_bucket *bucket;
	return __hmap_get(map, head, &bucket);
}

struct list_head *hmap_del(struct hmap *map, struct list_head *head);

static inline void hmap_term(struct hmap *map)
{
	free(map->buckets);
}

static inline int hmap_overloaded(struct hmap *map)
{
	// simulates load factor of 2
	return map->cnt >> 1 >= map->cap;
}

#define hmap_for_each(pos, map) \
	list_for_each(pos, &(map)->entries)

#define hmap_for_each_safe(pos, n, map) \
	list_for_each_safe(pos, n, &(map)->entries)

#endif // !__HMAP_H

/*
 * Copyright (c) 2024 helohekhelo
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
