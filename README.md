Simple separate-chaining hashmap implementation based on Linux Kernel
linked list.\
Same node can be used on different hashmaps.\
Hashmap can be turned into hashset easily.

# Usage
## Initializing hashmap
Use the init function:
```
void hmap_init(struct hmap *map, hmap_equal_f equal_fn, hmap_hash_f hash_fn);
```

Example:
```
// test struct
struct test_s {
    struct list_head head;
    int key;
    int val;
};

...
struct hmap map;
hmap_init(&map, cmp_fn, hash_fn);
...
```

Examples for equal and hash functions:
```
int cmp_fn(struct list_head *a_head, struct list_head *b_head)
{
    struct test_s *a = container_of(a_head, struct test_s, head);
    struct test_s *b = container_of(b_head, struct test_s, head);
    return a->key == b->key;
}

u32 hash_fn(struct list_head *head)
{
    return container_of(head, struct test_s, head)->key;
}
```

## Freeing hashmap
Use the terminate function:\
Does not free map if it was allocated via malloc.
```
void hmap_term(struct hmap *map);
```

## Adding
Use the add function:\
Expects that list_head's container was allocated via malloc.
```
struct list_head *hmap_add(struct hmap *map, struct list_head *head);
```

Example:
```
struct test_s *new_test_s(int key, int val)
{
    struct test_s *test = malloc(sizeof(*test));
    if (test == NULL) // hmap_add() doesn't perform null check
        return NULL;
    test->key = key;
    test->val = val;
    return test;
}

...
struct hmap map;
hmap_add(&map, &new_test_s(5, 15)->head);
...
```

## Deleting
Use the delete function:\
Expects a query list_head to be used:
```
struct list_head *hmap_del(struct hmap *map, struct list_head *head);
```

Example:
```
struct hmap map;
struct test_s query;
struct list_head *deleted;

query.key = 78;
deleted = hmap_del(&map, &query.head);
free(container_of(deleted, struct test_s, head));
```

## Retrieving
Use the get function:\
Expects a query list_head to be used:
```
struct list_head *hmap_get(struct hmap *map, struct list_head *head);
```

Example:
```
struct hmap map;
struct test_s query;
struct list_head *result;

query.key = 78;
result = hmap_del(&map, &query.head);
```

## Iterating
Iterates through internal linked list. Use this macro:
```
#define hmap_for_each(pos, map)
```

Safe against removal/free version:
```
#define hmap_for_each_safe(pos, map)
```

Example:
```
struct hmap map;
struct list_head *head;
struct test_s *test;

hmap_for_each(head, &map) {
    test = container_of(head, struct test_s, head);
    printf("%d, %d\n", test->key, test->val);
}

hmap_for_each_safe(head, &map) {
    test = container_of(head, struct test_s, head);
    free(test);
}
```

# Using in projects
Just copy over at least the hmap.c, hmap.h, list.h files into project root.
It should be easy to change to src/include project structure though. It does
depend on types/macros in def.h, so copy that or modify this code to support
your existing project structure.
