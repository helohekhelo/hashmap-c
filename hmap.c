#include "hmap.h"

#include <stdio.h>

static inline void hmap_default_buckets(size_t cap, struct hmap_bucket *buckets,
		struct list_head *entries)
{
	size_t i;

	for (i = 0; i < cap; ++i) {
		buckets->first = entries;
		buckets->last = entries;
		++buckets;
	}
}

void hmap_init(struct hmap *map, hmap_equal_f equal_fn, hmap_hash_f hash_fn)
{
	map->cap = HMAP_DEFAULT_BUCKETS;
	map->buckets = malloc(sizeof(*map->buckets) * map->cap);
	if (map->buckets == NULL) {
		fputs("hmap: malloc fail\n", stderr);
		exit(1);
	}

	map->cnt = 0;
	map->equal_fn = equal_fn;
	map->hash_fn = hash_fn;

	INIT_LIST_HEAD(&map->entries);
	hmap_default_buckets(map->cap, map->buckets, &map->entries);
}

static inline struct hmap_bucket *__hmap_get_bucket(size_t cap,
		struct hmap_bucket *buckets, u32 hash)
{
	return &buckets[hash & (cap - 1)];
}

static inline struct hmap_bucket *hmap_get_bucket(struct hmap *map, u32 hash)
{
	return __hmap_get_bucket(map->cap, map->buckets, hash);
}

static inline struct list_head *hmap_get_from_bucket(struct hmap *map, 
		struct hmap_bucket *bucket, struct list_head *head)
{
	struct list_head *node;

	for (node = bucket->first; node != bucket->last->next; node = node->next)
		if (map->equal_fn(head, node))
			return node;

	return NULL;
}

static inline void hmap_bucket_add(struct hmap_bucket *bucket,
		struct list_head *entries, struct list_head *head)
{
	list_add(head, bucket->last);
	if (bucket->first == entries)
		bucket->first = head;
	bucket->last = head;
}

static void hmap_resize(struct hmap *map)
{
	struct hmap_bucket *nbuckets, *bucket;
	struct list_head *node, *tmp;
	size_t ncap = map->cap << 1;

	nbuckets = malloc(sizeof(*nbuckets) * ncap);
	if (nbuckets == NULL) {
		fputs("hmap: malloc fail\n", stderr);
		exit(1);
	}
	
	hmap_default_buckets(ncap, nbuckets, &map->entries);
	list_for_each_safe(node, tmp, &map->entries) {
		list_del(node);
		bucket = __hmap_get_bucket(ncap, nbuckets, map->hash_fn(node));
		hmap_bucket_add(bucket, &map->entries, node);
	}

	free(map->buckets);
	map->buckets = nbuckets;
	map->cap = ncap;
}

struct list_head *hmap_add(struct hmap *map, struct list_head *head)
{
	struct hmap_bucket *bucket;
	struct list_head *node;

	node = __hmap_get(map, head, &bucket);
	if (node == NULL) {
		if (hmap_overloaded(map))
			hmap_resize(map);

		hmap_bucket_add(bucket, &map->entries, head);
		++map->cnt;
		return head;
	}
	
	return NULL;
}

struct list_head *__hmap_get(struct hmap *map,
		struct list_head *head, struct hmap_bucket **bucket)
{
	*bucket = hmap_get_bucket(map, map->hash_fn(head));
	if ((*bucket)->first == &map->entries)
		return NULL;

	return hmap_get_from_bucket(map, *bucket, head);
}

struct list_head *hmap_del(struct hmap *map, struct list_head *head)
{
	struct hmap_bucket *bucket;
	struct list_head *node;

	node = __hmap_get(map, head, &bucket);
	if (node == NULL)
		return NULL;

	if (bucket->first == bucket->last) {
		bucket->first = &map->entries;
		bucket->last = &map->entries;
	} else {
		if (node == bucket->first)
			bucket->first = node->next;

		if (node == bucket->last)
			bucket->last = node->prev;
	}

	list_del(node);
	--map->cnt;
	return node;
}

/*
 * Copyright (c) 2024 helohekhelo
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
